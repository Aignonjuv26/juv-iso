<?php

namespace ContainerCxBpEAS;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder61d2a = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializera4227 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties703fe = [
        
    ];

    public function getConnection()
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'getConnection', array(), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'getMetadataFactory', array(), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'getExpressionBuilder', array(), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'beginTransaction', array(), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->beginTransaction();
    }

    public function getCache()
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'getCache', array(), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->getCache();
    }

    public function transactional($func)
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'transactional', array('func' => $func), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->transactional($func);
    }

    public function commit()
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'commit', array(), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->commit();
    }

    public function rollback()
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'rollback', array(), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'getClassMetadata', array('className' => $className), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'createQuery', array('dql' => $dql), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'createNamedQuery', array('name' => $name), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'createQueryBuilder', array(), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'flush', array('entity' => $entity), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'clear', array('entityName' => $entityName), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->clear($entityName);
    }

    public function close()
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'close', array(), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->close();
    }

    public function persist($entity)
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'persist', array('entity' => $entity), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'remove', array('entity' => $entity), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'refresh', array('entity' => $entity), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'detach', array('entity' => $entity), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'merge', array('entity' => $entity), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'getRepository', array('entityName' => $entityName), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'contains', array('entity' => $entity), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'getEventManager', array(), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'getConfiguration', array(), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'isOpen', array(), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'getUnitOfWork', array(), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'getProxyFactory', array(), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'initializeObject', array('obj' => $obj), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'getFilters', array(), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'isFiltersStateClean', array(), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'hasFilters', array(), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return $this->valueHolder61d2a->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializera4227 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder61d2a) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder61d2a = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder61d2a->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, '__get', ['name' => $name], $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        if (isset(self::$publicProperties703fe[$name])) {
            return $this->valueHolder61d2a->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder61d2a;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder61d2a;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, '__set', array('name' => $name, 'value' => $value), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder61d2a;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder61d2a;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, '__isset', array('name' => $name), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder61d2a;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder61d2a;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, '__unset', array('name' => $name), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder61d2a;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder61d2a;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, '__clone', array(), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        $this->valueHolder61d2a = clone $this->valueHolder61d2a;
    }

    public function __sleep()
    {
        $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, '__sleep', array(), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;

        return array('valueHolder61d2a');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializera4227 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializera4227;
    }

    public function initializeProxy() : bool
    {
        return $this->initializera4227 && ($this->initializera4227->__invoke($valueHolder61d2a, $this, 'initializeProxy', array(), $this->initializera4227) || 1) && $this->valueHolder61d2a = $valueHolder61d2a;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder61d2a;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder61d2a;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
