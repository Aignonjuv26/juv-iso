<?php

namespace App\Controller\Client;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/client/dashboard")
 */
class DashboardController extends AbstractController
{
     #[Route('/', name: 'client_dashboard')]
    public function index(): Response
    {
         return $this->render('client/dasboard/index.html.twig',[
           'controller_name' => 'DashboardController',
        ]);
    }
}
