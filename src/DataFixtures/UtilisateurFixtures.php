<?php

namespace App\DataFixtures;

use App\Entity\Utilisateur;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
//use Symfony\Component\Security\Core\User\UserInterface;

class UtilisateurFixtures extends Fixture
{
    private UserPasswordEncoderInterface $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder){

        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $client = new Utilisateur();
        $client->setEmail('client@gmail.com');
        $client->setPassword($this->passwordEncoder->encodePassword($client, "123456"));
        $client->setRoles(["ROLE_CLIENT"]);


        $fournisseur = new Utilisateur();
        $fournisseur->setEmail('fournisseur@gmail.com');
        $fournisseur->setPassword($this->passwordEncoder->encodePassword($fournisseur, "123456"));
        $fournisseur->setRoles(["ROLE_FOURNISSEUR"]);

        $admin = new Utilisateur();
        $admin->setEmail('admin@gmail.com');
        $admin->setPassword($this->passwordEncoder->encodePassword($admin, "123456"));
        $admin->setRoles(["ROLE_ADMIN"]);


        $manager->persist($client);
        $manager->persist($fournisseur);
        $manager->persist($admin);
        $manager->flush();
    }
}
