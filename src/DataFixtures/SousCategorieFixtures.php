<?php

namespace App\DataFixtures;
use App\Entity\SousCategorie;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class SousCategorieFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        for ($j = 1; $j <= 5; $j++) {
            $souscategorie = new SousCategorie();
            $souscategorie->setLibelle("Sous Categorie" . $j);

            //appel de la reference
            $souscategorie->setCategorie($this->getReference(CategorieFixtures::CATEGORIE_REFERENCE));
            $manager->persist($souscategorie);
        }
        $manager->flush();
    }
    public function getDependencies()
    {
        return array(
            CategorieFixtures::class,
        );
    }
}
